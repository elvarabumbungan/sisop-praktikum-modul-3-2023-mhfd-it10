#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#define kuantitas_ 2000

struct Message {
    long messageType;
    char output_teks[kuantitas_];
};

int main() {
    int msgid = msgget(ftok(".", 's'), 0666);
    if (msgid == -1) {
        perror("msgget");
        exit(1);
    }

    printf("Selamat datang di sistem stream.\n");
    printf("Untuk melihat daftar perintah, silakan masukkan GO.\n");

    for (;;) {
        char input[kuantitas_];
        printf("\nPerintah: ");
        fgets(input, kuantitas_, stdin);

        // hapus newline character dari input
        input[strcspn(input, "\n")] = 0;

        struct Message msg;
        msg.messageType = 1;
        strncpy(msg.output_teks, input, kuantitas_);

        if (msgsnd(msgid, &msg, sizeof(msg.output_teks), 0) == -1) {
            perror("msgsnd");
            exit(1);
        }

        if (strcmp(input, "GO") == 0) {
            printf("Daftar perintah:\n");
            printf("- DECRYPT: untuk melakukan decrypt pada file song-playlist.json\n");
            printf("- LIST: untuk menampilkan daftar lagu yang telah di-decrypt\n");
            printf("- EXIT: untuk keluar dari program\n");
        } else if (strcmp(input, "EXIT") == 0) {
            printf("Sampai jumpa!\n");
            break;
        }
    }

    return 0;
}
