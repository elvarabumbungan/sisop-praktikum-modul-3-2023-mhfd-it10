#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define SHM_KEY 1412

int main() {
   int matone[4][2], mattwo[2][5], output[4][5];

      srand(time(NULL));
   printf("matriks 1:\n");
   for(int i=0; i<4; i++) {
      for(int j=0; j<2; j++) {
         matone[i][j] = rand() % 5 + 1;
         printf("%d ", matone[i][j]);
      }
      printf("\n");
   }

     printf("\nmatriks 2:\n");
   for(int i=0; i<2; i++) {
      for(int j=0; j<5; j++) {
         mattwo[i][j] = rand() % 4 + 1;
         printf("%d ", mattwo[i][j]);
      }
      printf("\n");
   }

   for(int i=0; i<4; i++) {
      for(int j=0; j<5; j++) {
         int sum = 0;
         for(int k=0; k<2; k++) {
            sum += matone[i][k] * mattwo[k][j];
         }
         output[i][j] = sum;
      }
   }

    printf("\nhasil di dapat:\n");
   for(int i=0; i<4; i++) {
      for(int j=0; j<5; j++) {
         printf("%d ", output[i][j]);
      }
      printf("\n");
   }

 
   int sd = shmget(SHM_KEY, sizeof(int[4][5]), IPC_CREAT | 0666);
   if(sd == -1) {
      perror("shmget");
      exit(1);
   }
   
   int (*hasil)[5];
   hasil = shmat(sd, NULL, 0);
   if(hasil == (int (*)[5]) -1) {
      perror("shmat");
      exit(1);
   }
   
      for(int i=0; i<4; i++) {
      for(int j=0; j<5; j++) {
         hasil[i][j] = output[i][j];
      }
   }

   shmdt(hasil);

   return 0;
}