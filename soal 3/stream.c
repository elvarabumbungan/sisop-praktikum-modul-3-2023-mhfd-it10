#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <math.h>
#include <unistd.h>

#define kuantitas_lagu 2000
#define kuantitas_ 2000

struct Message {
    long messageType;
    char output_teks[kuantitas_];
};

int metoderot13(char *order) {
    int indeks_ = 0;
    char kata_;

    while ((kata_ = order[indeks_++])) {
        if (kata_ >= 'A' && kata_ <= 'Z') {
            kata_ = ((kata_ - 'A' + 13) % 26) + 'A';
        } else if (kata_ >= 'a' && kata_ <= 'z') {
            kata_ = ((kata_ - 'a' + 13) % 26) + 'a';
        }
        order[indeks_-1] = kata_;
    }

    return 0;
}

int charbase64(char c) {
    if (c >= 'A' && c <= 'Z') return c - 'A';
    if (c >= 'a' && c <= 'z') return c - 'a' + 26;
    if (c >= '0' && c <= '9') return c - '0' + 52;
    if (c == '+') return 62;
    if (c == '/') return 63;
    return -1;  
}

int metodebase64(char *teks) {
    int i = 0, j = 0;
    while (teks[i]) {
        int nilai = charbase64(teks[i++]);
        if (nilai != -1) teks[j++] = teks[i-1];
    }
    teks[j] = '\0';

    int len = strlen(teks);
    int man = (teks[len - 1] == '=') + (teks[len - 2] == '=');

    len = len * 3 / 4 - man;

    char *out = calloc(len + 1, sizeof(char));

    for (i = 0, j = 0; i < len; i += 3, j += 4) {
        unsigned long n = (charbase64(teks[j]) << 18) | (charbase64(teks[j+1]) << 12) | (charbase64(teks[j+2]) << 6) | charbase64(teks[j+3]);
        out[i] = (n >> 16) & 0xFF;
        out[i+1] = (n >> 8) & 0xFF;
        out[i+2] = n & 0xFF;
    }

    memcpy(teks, out, len);
    teks[len] = '\0';

    free(out);

    return 0;
}


int metodehex(char *teks) {
    int i = 0, j = 0;
    size_t len = strlen(teks);

    for (i = 0; i < len; i += 2) {
        int go = 0;

        for (int k = 0; k < 2 && isxdigit(teks[i + k]); k++) {
            go = (go << 4) | (isdigit(teks[i+k]) ? teks[i+k]-'0' : tolower(teks[i+k])-'a'+10);
        }

        teks[j++] = go & 0xFF;
    }

    teks[j] = '\0';

    return 0;
}


typedef int (*DecryptMethod)(char*);
const DecryptMethod metodedekrip[] = {
    metoderot13,
    metodebase64,
    metodehex
};

const char *jenismetode[] = {
    "rot13",
    "base64",
    "hex"
};

int mendekriplagu(char *method, char *song) {
    
    int i;
    for (i = 0; i < sizeof(jenismetode) / sizeof(jenismetode[0]); i++) {
        if (strcmp(method, jenismetode[i]) == 0) {
            break;
        }
    }
   
   
    if (i < sizeof(jenismetode) / sizeof(jenismetode[0])) {
        return metodedekrip[i](song);
    } else {
        printf("Metode tidak diketahui: %s\n", method);
        return -1;
    }
}

char songs[kuantitas_lagu][kuantitas_];

int bandingkan(const void *a, const void *b) {
    return strcmp((const char *)a, (const char *)b);
}

int filesort() {
    FILE *Input_file, *Output_file;
    char line[kuantitas_];
    char method[kuantitas_];
    char song[kuantitas_];
    char key[] = " \"method\": \"";
    char keyEnd[] = "\",";
    int countlagu = 0;

    Input_file = fopen("song-playlist.json", "r");
    if (Input_file == NULL) {
        perror("gagal membuka song-playlist.json ");
        return -1;
    }

    while (fgets(line, sizeof(line), Input_file) != NULL) {
       
        char *pMethod = strstr(line, key);
        if (pMethod == NULL) {
            continue;
        }
        
        strncpy(method, pMethod + strlen(key), kuantitas_);
       
        char *pQuote = strstr(method, keyEnd);
        if (pQuote == NULL) {
            continue;
        }
        
        *pQuote = '\0';

    
        char *pSong = strstr(line, " \"song\": \"");
        if (pSong == NULL) {
            continue;
        }
        
        strncpy(song, pSong + strlen(" \"song\": \""), kuantitas_);
      
        pQuote = strstr(song, "\"");
        if (pQuote == NULL) {
            continue;
        }
      
        *pQuote = '\0';

       
        mendekriplagu(method, song);

       
        strncpy(songs[countlagu], song, kuantitas_);
        countlagu++;
    }

    fclose(Input_file);

    
    if (countlagu == 0) {
        printf("Tidak ada lagu yang bisa didekripsi.\n");
        return 0;
    }

   
    qsort(songs, countlagu, kuantitas_ * sizeof(char), bandingkan);

    Output_file = fopen("playlist.txt", "w");
    if(Output_file == NULL) {
        perror("tidak bisa membuka playlist.txt");
        return -1;
    }

    
    fprintf(Output_file, "Playlist yang telah di dekrip:\n");
    for (int i = 0; i < countlagu; i++) {
        fprintf(Output_file, "- %s\n", songs[i]);
    }
    fclose(Output_file);

    printf("Playlist masuk 'playlist.txt'\n");

    return 0;
}

int perintahlist() {
    const char* nama_file = "playlist.txt";
    char buffer[kuantitas_];
    memset(buffer, 0, sizeof(buffer));

    FILE *Output_file = fopen(nama_file, "r");
    if (!Output_file) {
        printf("Tidak bisa membuka file %s\n", nama_file);
        return -1;
    }

    int lineCount = 0;
    while (fgets(buffer, kuantitas_, Output_file)) {
        lineCount++;
    }

    printf("Daftar Lagu terdekripsi (%d):\n", lineCount);

    rewind(Output_file); 
    while (fgets(buffer, kuantitas_, Output_file)) {
        printf("%s", buffer);
    }

    fclose(Output_file);
    return 0;
}

int perintahTidakDikenal() {
    printf("UNKNOWN COMMAND\n");
    return 0;
}

int main() {
    int msgid = msgget(ftok(".", 's'), 0666 | IPC_CREAT);
    if (msgid == -1) {
        perror("msgget");
        exit(1);
    }

    for (;;) {
        struct Message msg;
        msgrcv(msgid, &msg, sizeof(msg.output_teks), 1, 0);

        puts("Pesan diterima:");
        switch (msg.output_teks[0]) {
            case 'D':
                if (filesort() != 0) {
                    puts("Gagal membuat file playlist.");
                }
                break;
            case 'L':
                if (perintahlist() != 0) {
                    puts("Gagal menampilkan daftar lagu.");
                }
                break;
            default:
                puts("Pilihan tidak dapat dilihat.");
        }
    }
    return 0;
} 
