#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#define SHM_KEY 1412

int (*matriks)[5];
long double (*hasil)[5];

void *factorial(void *arg) {
int nomor = *((int *) arg);
long double fact = 1;
for(int i=1; i<=nomor; i++) {
fact *= i;
}
long double *output = malloc(sizeof(long double));
*output = fact;
return (void *) output;
}

int main() {
int sd = shmget(SHM_KEY, sizeof(int[4][5]), 0666);
if(sd == -1) {
perror("kesalahan shmget");
exit(1);
}

matriks = (int (*)[5]) shmat(sd, NULL, 0);
if(matriks == (int (*)[5]) -1) {
    perror("kesalahan shmat");
    exit(1);
}


int sd2 = shmget(IPC_PRIVATE, sizeof(long double[4][5]), IPC_CREAT | 0666);
if(sd2 == -1) {
    perror("kesalahan shmget");
    exit(1);
}

hasil = (long double (*)[5]) shmat(sd2, NULL, 0);
if(hasil == (long double (*)[5]) -1) {
    perror("kesalahan shmat");
    exit(1);
}


pthread_t threads[20];
int argthreads0[20], countThr = 0;

printf("perkalian matriks kalian.c :\n");

struct timespec go, finish;
clock_gettime(CLOCK_MONOTONIC, &go);

for(int i=0; i<4; i++) {
    for(int j=0; j<5; j++) {
        printf("%d ", matriks[i][j]);
        argthreads0[countThr] = matriks[i][j];
        int rc = pthread_create(&threads[countThr], NULL, factorial, &argthreads0[countThr]);
        if(rc) {
            perror("pthread_create");
            exit(1);
        }
        void *output;
        pthread_join(threads[countThr], &output); 
        hasil[i][j] = *((long double *) output);
        free(output);
        countThr++;
    }
    printf("\n");
}

clock_gettime(CLOCK_MONOTONIC, &finish); 
long long elapsed_ns = (finish.tv_sec - go.tv_sec) * 1000000000 + finish.tv_nsec - go.tv_nsec;
printf("\nWaktu : %lld ns\n", elapsed_ns);


printf("\nfaktorial :\n");
for(int i=0; i<4; i++) {
    for(int j=0; j<5; j++) {
        printf("%.0Lf ", hasil[i][j]);
    }
    printf("\n");
}

// detach and remove shared memory segments
shmdt(matriks);
shmdt(hasil);
shmctl(sd2, IPC_RMID, NULL);

return 0;}