#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <errno.h>

void download_file(char* url, char* filename) {
    pid_t pid = fork();
    if (pid == 0) {
        // child process
        execlp("wget", "wget", "-O", filename, url, NULL);
        exit(EXIT_SUCCESS);
    } else if (pid > 0) {
        // parent process
        int status;
        waitpid(pid, &status, 0);
    } else {
        // fork failed
        perror("fork failed");
        exit(EXIT_FAILURE);
    }
}

void unzip_file(char *filename) {
    pid_t pid = fork();
    if (pid == 0) {
        execlp("unzip", "unzip", filename, NULL);
        perror("Failed to unzip file");
        exit(1);
    } else if (pid < 0) {
        perror("Failed to create child process");
        exit(1);
    } else {
        int status;
        waitpid(pid, &status, 0);
        if (WIFEXITED(status) && WEXITSTATUS(status) != 0) {
            exit(1);
        }
    }
}

int main() {
    char *url = "https://drive.google.com/uc?export=download&id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp";
    char *filename = "hehe.zip";

    download_file(url, filename);
    unzip_file(filename);

    return 0;
}
