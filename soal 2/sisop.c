#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#define SHM_KEY 12345

int main() {
   
    int sd = shmget(SHM_KEY, sizeof(int[4][5]), 0666);
    if(sd == -1) {
        perror("kesalahan shmget");
        exit(1);
    }

    int (*matriks)[5];
    matriks = (int (*)[5]) shmat(sd, NULL, 0);
    if(matriks == (int (*)[5]) -1) {
        perror("kesalahan shmat");
        exit(1);
    }

    int sd2 = shmget(IPC_PRIVATE, sizeof(long double[4][5]), IPC_CREAT | 0666);
    if(sd2 == -1) {
        perror("kesalahan shmget");
        exit(1);
    }

    long double (*hasil)[5];
    hasil = (long double (*)[5]) shmat(sd2, NULL, 0);
    if(hasil == (long double (*)[5]) -1) {
        perror("kesalahan shmat");
        exit(1);
    }
   
    printf("perkalian matriks kalian.c :\n");
    struct timespec go, finish;
    clock_gettime(CLOCK_MONOTONIC, &go); 
   
    for(int i=0; i<4; i++) {
        for(int j=0; j<5; j++) {
            printf("%d ", matriks[i][j]);
            long double fact = 1;
            for(int k=1; k<=matriks[i][j]; k++) {
                fact *= k;
            }
            hasil[i][j] = fact;
        }
        printf("\n");
    }

    clock_gettime(CLOCK_MONOTONIC, &finish); 
    long long elapsed_ns = (finish.tv_sec - go.tv_sec) * 1000000000 + finish.tv_nsec - go.tv_nsec;
    printf("\nWaktu : %lld ns\n", elapsed_ns);


    printf("\nfaktorial yang di dapat:\n");
    for(int i=0; i<4; i++) {
        for(int j=0; j<5; j++) {
            printf("%.0Lf ", hasil[i][j]);
        }
        printf("\n");
    }

 
    shmdt(matriks);
    shmdt(hasil);
    shmctl(sd2, IPC_RMID, NULL);

    return 0;
}