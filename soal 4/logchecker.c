#include <stdio.h>
#include <string.h>

int countAccessedEvents(char *filename) {
    char accessed[] = "ACCESSED";
    char line[100];
    int count = 0;
    
    FILE* file = fopen(filename, "r");
    if (file == NULL) {
        printf("Error opening file\n");
        return -1;
    }
    
    while (fgets(line, sizeof(line), file)) {
        if (strstr(line, accessed)) {
            count++;
        }
    }
    
    fclose(file);
    
    return count;
}

int main() {
    char filename[] = "log.txt";
    int count = countAccessedEvents(filename);
    
    if (count >= 0) {
        printf("Number of ACCESSED events: %d\n", count);
    }
    
    return 0;
}
