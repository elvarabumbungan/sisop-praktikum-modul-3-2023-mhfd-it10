# sisop-praktikum-modul-3-2023-MHFD-IT10

# Soal 1

 __Analisa Soal__

Pada soal ini diminta untuk membuat program huffman yang dimana bisa mengkompresi sebuah file berdasarkan frekuensi huruf kemudian mengubah char menjadi huffman code

__Code Progam__


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <sys/wait.h>
#include <ctype.h>

#define MAX_TREE_HT 50

struct MinHNode
{
    char item;
    unsigned freq;
    struct MinHNode *left, *right;
};

struct MinHeap
{
    unsigned size;
    unsigned capacity;
    struct MinHNode **array;
};

struct MinHNode *newNode(char item, unsigned freq)
{
    struct MinHNode *temp = (struct MinHNode *)malloc(sizeof(struct MinHNode));

    temp->left = temp->right = NULL;
    temp->item = item;
    temp->freq = freq;

    return temp;
}

int isLeaf(struct MinHNode *root)
{
    return !(root->left) && !(root->right);
}

char letDec(struct MinHNode *node, int *index, char *str, char res)
{
    while (node->left != NULL || node->right != NULL)
    {
        if (str[*index] == '0')
        {
            node = node->left;
        }
        else
        {
            node = node->right;
        }
        (*index)++;
    }
    return node->item;
}

void textDec(char *str, struct MinHNode *node, char *text)
{
    int index = 0;
    while (index < strlen(str))
    {
        struct MinHNode *current = node;
        while (current->left != NULL || current->right != NULL)
        {
            if (str[index] == '0')
            {
                current = current->left;
            }
            else
            {
                current = current->right;
            }
            index++;
        }
        char resString[2];
        resString[0] = current->item;
        resString[1] = '\0';
        strcat(text, resString);
    }
}

void textEnc(char *str, char **huffmanCode)
{
    FILE *fp;
    char fileName[] = "file.txt";
    fp = fopen(fileName, "r");
    if (fp == NULL)
    {
        printf("Gagal Membuka File\n");
        return;
    }

    int c;
    while ((c = fgetc(fp)) != EOF)
    {
        if (isalpha(c))
        {
            c = toupper(c);
            strcat(str, huffmanCode[c - 'A']);
        }
    }

    fclose(fp);
}

void getSBT(struct MinHNode *node, char *str, int *index, int isRight)
{
    if (node == NULL)
    {
        return;
    }

    str[(*index)++] = node->item;

    if (node->left == NULL && node->right == NULL)
    {
        return;
    }

    str[(*index)++] = '(';
    getSBT(node->left, str, index, 1);
    str[(*index)++] = ')';

    if (node->right != NULL)
    {
        str[(*index)++] = '(';
        getSBT(node->right, str, index, 1);
        str[(*index)++] = ')';
    }
}

struct MinHNode *stringToTree(char *str, int *index)
{
    if (str[*index] == ')')
    {
        return NULL;
    }

    struct MinHNode *node = newNode(str[*index], 0);

    (*index)++;

    if (str[*index] != '(')
    {
        (*index)++;
        return node;
    }

    (*index)++;
    node->left = stringToTree(str, index);

    (*index)++;
    node->right = stringToTree(str, index);

    (*index)++;

    return node;
}

void getHuffCodes(struct MinHNode *root, int arr[], int top, char **huffmanCode)
{
    if (root->left)
    {
        arr[top] = 0;
        getHuffCodes(root->left, arr, top + 1, huffmanCode);
    }
    if (root->right)
    {
        arr[top] = 1;
        getHuffCodes(root->right, arr, top + 1, huffmanCode);
    }
    if (isLeaf(root))
    {
        char *binary = (char *)malloc((top + 1) * sizeof(char));
        for (int i = 0; i < top; i++)
        {
            binary[i] = arr[i] + '0';
        }
        binary[top] = '\0';
        int idx = root->item - 65;
        printf("  %d %c   |  %s\n", idx, root->item, binary);
        huffmanCode[idx] = binary;
    }
}

void inorderTraversal(struct MinHNode *root)
{
    if (root == NULL)
    {
        return;
    }
    inorderTraversal(root->left);
    printf("%c ", root->item);
    inorderTraversal(root->right);
}

struct MinHeap *createMinH(unsigned capacity)
{
    struct MinHeap *minHeap = (struct MinHeap *)malloc(sizeof(struct MinHeap));

    minHeap->size = 0;

    minHeap->capacity = capacity;

    minHeap->array = (struct MinHNode **)malloc(minHeap->capacity * sizeof(struct MinHNode *));
    return minHeap;
}

void swapMinHNode(struct MinHNode **a, struct MinHNode **b)
{
    struct MinHNode *t = *a;
    *a = *b;
    *b = t;
}

void minHeapify(struct MinHeap *minHeap, int idx)
{
    int smallest = idx;
    int left = 2 * idx + 1;
    int right = 2 * idx + 2;

    if (left < minHeap->size && minHeap->array[left]->freq < minHeap->array[smallest]->freq)
        smallest = left;

    if (right < minHeap->size && minHeap->array[right]->freq < minHeap->array[smallest]->freq)
        smallest = right;

    if (smallest != idx)
    {
        swapMinHNode(&minHeap->array[smallest], &minHeap->array[idx]);
        minHeapify(minHeap, smallest);
    }
}

int checkSizeOne(struct MinHeap *minHeap)
{
    return (minHeap->size == 1);
}

struct MinHNode *extractMin(struct MinHeap *minHeap)
{
    struct MinHNode *temp = minHeap->array[0];
    minHeap->array[0] = minHeap->array[minHeap->size - 1];

    --minHeap->size;
    minHeapify(minHeap, 0);

    return temp;
}

void insertMinHeap(struct MinHeap *minHeap, struct MinHNode *minHeapNode)
{
    ++minHeap->size;
    int i = minHeap->size - 1;

    while (i && minHeapNode->freq < minHeap->array[(i - 1) / 2]->freq)
    {
        minHeap->array[i] = minHeap->array[(i - 1) / 2];
        i = (i - 1) / 2;
    }
    minHeap->array[i] = minHeapNode;
}

void buildMinHeap(struct MinHeap *minHeap)
{
    int n = minHeap->size - 1;
    int i;

    for (i = (n - 1) / 2; i >= 0; --i)
        minHeapify(minHeap, i);
}

struct MinHeap *createAndBuildMinHeap(char item[], int freq[], int size)
{
    struct MinHeap *minHeap = createMinH(size);

    for (int i = 0; i < size; ++i)
        minHeap->array[i] = newNode(item[i], freq[i]);

    minHeap->size = size;
    buildMinHeap(minHeap);

    return minHeap;
}

struct MinHNode *buildHuffmanTree(char item[], int freq[], int size)
{
    struct MinHNode *left, *right, *top;
    struct MinHeap *minHeap = createAndBuildMinHeap(item, freq, size);

    while (!checkSizeOne(minHeap))
    {
        left = extractMin(minHeap);
        right = extractMin(minHeap);

        top = newNode('$', left->freq + right->freq);

        top->left = left;
        top->right = right;

        insertMinHeap(minHeap, top);
    }
    return extractMin(minHeap);
}

char *getBinaryCode(int arr[], int n)
{
    char *res = (char *)malloc(sizeof(char) * (n + 1));
    int i;
    for (i = 0; i < n; ++i)
    {
        res[i] = arr[i] + '0';
    }

    return res;
}

void printArray(int arr[], int n)
{
    int i;
    for (i = 0; i < n; ++i)
        printf("%d", arr[i]);

    printf("\n");
}

void printHCodes(struct MinHNode *root, int arr[], int top)
{
    if (root->left)
    {
        arr[top] = 0;
        printHCodes(root->left, arr, top + 1);
    }
    if (root->right)
    {
        arr[top] = 1;
        printHCodes(root->right, arr, top + 1);
    }
    if (isLeaf(root))
    {
        printf("  %c   | ", root->item);
        printArray(arr, top);
    }
}

struct MinHNode HuffCodes(char item[], int freq[], int size)
{
    struct MinHNode *root = buildHuffmanTree(item, freq, size);

    int arr[MAX_TREE_HT], top = 0;

    return *root;
}

int main()
{
    FILE *fp;
    char fileName[] = "file.txt";
    fp = fopen(fileName, "r");
    if (fp == NULL)
    {
        printf("Gagal membuka file\n");
        return 1;
    }

    int freq[26] = {0};
    int c;
    int letterCount = 0;
    while ((c = fgetc(fp)) != EOF)
    {
        if (isalpha(c))
        {
            letterCount++;
            c = toupper(c);
            freq[c - 'A']++;
        }
    }

    fclose(fp);

    char letters[30];
    int fq[30];

    int currIdx = 0;
    for (int i = 0; i < 26; i++)
    {
        if (freq[i] > 0)
        {
            letters[currIdx] = 'A' + i;
            fq[currIdx] = freq[i];
            currIdx++;
        }
    }

    int fd1[2];
    int fd2[2];

    if (pipe(fd1) == -1 || pipe(fd2) == -1)
    {
        perror("pipe");
        return 1;
    }

    pid_t c_pid = fork();

    if (c_pid < 0)
    {
        fprintf(stderr, "Gagal fork");
        return 1;
    }

    if (c_pid > 0)
    {
        wait(NULL);
        printf("\n");
        printf("Parent Proccess\n");
        close(fd1[1]);
        close(fd2[1]);

        char *stringTree = (char *)malloc(sizeof(char) * 150);
        char *encodedText = (char *)malloc(sizeof(char) * 3000);

        read(fd1[0], stringTree, 150);
        read(fd2[0], encodedText, 3000);

        int idxTree = 0;
        struct MinHNode *root = stringToTree(stringTree, &idxTree);

        char **huffmanCode = (char **)malloc(sizeof(char) * 10 * 30);

        char *text = (char *)malloc(sizeof(char) * 1000);
        textDec(encodedText, root, text);
        printf("%s\n", text);
        printf("\n");

        printf("Membandingkan ...\n");
        int originalFileSizeBits = letterCount * 8;
        int compressedFileSizeBits = strlen(text);

        printf("File asli  : %d \n", originalFileSizeBits);
        printf("Teks terkompres  : %d \n", compressedFileSizeBits);
    }

    if (c_pid == 0)
    {
        close(fd2[0]);
        close(fd1[0]);

        printf("Membuat huffman tree ... \n");
        struct MinHNode tree = HuffCodes(letters, fq, currIdx);
        struct MinHNode *root = &tree;
        char *str = (char *)malloc(sizeof(char) * 150);
        int index = 0;
        getSBT(root, str, &index, 0);
        str[index] = '\0';
        printf("%s\n", str);
        write(fd1[1], str, strlen(str) + 1);

        printf("Membuat huffman code ... \n");
        char **huffmanCode = (char **)malloc(sizeof(char) * 10 * 30);

        int arr[MAX_TREE_HT], top = 0;
        getHuffCodes(root, arr, top, huffmanCode);

        for (int i = 0; i < 22; i++)

            printf("Mengencode teks ... \n");
        char *encodedText = (char *)malloc(sizeof(char) * 3000);
        textEnc(encodedText, huffmanCode);
        printf("%s\n", encodedText);

        write(fd2[1], encodedText, strlen(encodedText) + 1);
    }
}

__Output__
![O1](https://i.ibb.co/ZWb8d2p/message-Image-1683894003245.jpg)
![O1](https://i.ibb.co/VvZ33xP/SISOPPPPPPPPPPP.jpg)


__Kendala__
kesusahan mengerti soal karena sempat bingung dekompresi yang mana

# Soal 2 

 __Analisa Soal__


Pada soal ini diminta membuat 3 progam c yaitu kalian.c , cinta.c, dan sisop.c. Progam kalian.c berisi progam perkalian matriks 4x2 dan 2x5 yang berisi angka random (1-5) dan (1-4). Progam cinta.c berisi berisi pengambilan data dari kalian.c dan hasil faktorial dari matriks yang dikalikan dengan menggunakan thread dan multithread. Progam sisop.c berisi sama dengan cinta.c namun tanpa menggunakan thread dan multithread. 


__Code Progam__

1. kalian.c
``` 
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define SHM_KEY 1412

int main() {
   int matone[4][2], mattwo[2][5], output[4][5];

      srand(time(NULL));
   printf("matriks 1:\n");
   for(int i=0; i<4; i++) {
      for(int j=0; j<2; j++) {
         matone[i][j] = rand() % 5 + 1;
         printf("%d ", matone[i][j]);
      }
      printf("\n");
   }

     printf("\nmatriks 2:\n");
   for(int i=0; i<2; i++) {
      for(int j=0; j<5; j++) {
         mattwo[i][j] = rand() % 4 + 1;
         printf("%d ", mattwo[i][j]);
      }
      printf("\n");
   }

   for(int i=0; i<4; i++) {
      for(int j=0; j<5; j++) {
         int sum = 0;
         for(int k=0; k<2; k++) {
            sum += matone[i][k] * mattwo[k][j];
         }
         output[i][j] = sum;
      }
   }

    printf("\nhasil di dapat:\n");
   for(int i=0; i<4; i++) {
      for(int j=0; j<5; j++) {
         printf("%d ", output[i][j]);
      }
      printf("\n");
   }

 
   int sd = shmget(SHM_KEY, sizeof(int[4][5]), IPC_CREAT | 0666);
   if(sd == -1) {
      perror("shmget");
      exit(1);
   }
   
   int (*hasil)[5];
   hasil = shmat(sd, NULL, 0);
   if(hasil == (int (*)[5]) -1) {
      perror("shmat");
      exit(1);
   }
   
      for(int i=0; i<4; i++) {
      for(int j=0; j<5; j++) {
         hasil[i][j] = output[i][j];
      }
   }

   shmdt(hasil);

   return 0;
}

```

2. cinta.c
```
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#define SHM_KEY 1412

int (*matriks)[5];
long double (*hasil)[5];

void *factorial(void *arg) {
int nomor = *((int *) arg);
long double fact = 1;
for(int i=1; i<=nomor; i++) {
fact *= i;
}
long double *output = malloc(sizeof(long double));
*output = fact;
return (void *) output;
}

int main() {
int sd = shmget(SHM_KEY, sizeof(int[4][5]), 0666);
if(sd == -1) {
perror("kesalahan shmget");
exit(1);
}

matriks = (int (*)[5]) shmat(sd, NULL, 0);
if(matriks == (int (*)[5]) -1) {
    perror("kesalahan shmat");
    exit(1);
}


int sd2 = shmget(IPC_PRIVATE, sizeof(long double[4][5]), IPC_CREAT | 0666);
if(sd2 == -1) {
    perror("kesalahan shmget");
    exit(1);
}

hasil = (long double (*)[5]) shmat(sd2, NULL, 0);
if(hasil == (long double (*)[5]) -1) {
    perror("kesalahan shmat");
    exit(1);
}


pthread_t threads[20];
int argthreads0[20], countThr = 0;

printf("perkalian matriks kalian.c :\n");

struct timespec go, finish;
clock_gettime(CLOCK_MONOTONIC, &go);

for(int i=0; i<4; i++) {
    for(int j=0; j<5; j++) {
        printf("%d ", matriks[i][j]);
        argthreads0[countThr] = matriks[i][j];
        int rc = pthread_create(&threads[countThr], NULL, factorial, &argthreads0[countThr]);
        if(rc) {
            perror("pthread_create");
            exit(1);
        }
        void *output;
        pthread_join(threads[countThr], &output); 
        hasil[i][j] = *((long double *) output);
        free(output);
        countThr++;
    }
    printf("\n");
}

clock_gettime(CLOCK_MONOTONIC, &finish); 
long long elapsed_ns = (finish.tv_sec - go.tv_sec) * 1000000000 + finish.tv_nsec - go.tv_nsec;
printf("\nWaktu : %lld ns\n", elapsed_ns);


printf("\nfaktorial :\n");
for(int i=0; i<4; i++) {
    for(int j=0; j<5; j++) {
        printf("%.0Lf ", hasil[i][j]);
    }
    printf("\n");
}

// detach and remove shared memory segments
shmdt(matriks);
shmdt(hasil);
shmctl(sd2, IPC_RMID, NULL);

return 0;}

```

3. sisop.c
```

#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#define SHM_KEY 12345

int main() {
   
    int sd = shmget(SHM_KEY, sizeof(int[4][5]), 0666);
    if(sd == -1) {
        perror("kesalahan shmget");
        exit(1);
    }

    int (*matriks)[5];
    matriks = (int (*)[5]) shmat(sd, NULL, 0);
    if(matriks == (int (*)[5]) -1) {
        perror("kesalahan shmat");
        exit(1);
    }

    int sd2 = shmget(IPC_PRIVATE, sizeof(long double[4][5]), IPC_CREAT | 0666);
    if(sd2 == -1) {
        perror("kesalahan shmget");
        exit(1);
    }

    long double (*hasil)[5];
    hasil = (long double (*)[5]) shmat(sd2, NULL, 0);
    if(hasil == (long double (*)[5]) -1) {
        perror("kesalahan shmat");
        exit(1);
    }
   
    printf("perkalian matriks kalian.c :\n");
    struct timespec go, finish;
    clock_gettime(CLOCK_MONOTONIC, &go); 
   
    for(int i=0; i<4; i++) {
        for(int j=0; j<5; j++) {
            printf("%d ", matriks[i][j]);
            long double fact = 1;
            for(int k=1; k<=matriks[i][j]; k++) {
                fact *= k;
            }
            hasil[i][j] = fact;
        }
        printf("\n");
    }

    clock_gettime(CLOCK_MONOTONIC, &finish); 
    long long elapsed_ns = (finish.tv_sec - go.tv_sec) * 1000000000 + finish.tv_nsec - go.tv_nsec;
    printf("\nWaktu : %lld ns\n", elapsed_ns);


    printf("\nfaktorial yang di dapat:\n");
    for(int i=0; i<4; i++) {
        for(int j=0; j<5; j++) {
            printf("%.0Lf ", hasil[i][j]);
        }
        printf("\n");
    }

 
    shmdt(matriks);
    shmdt(hasil);
    shmctl(sd2, IPC_RMID, NULL);

    return 0;
}


```

__Testing Output__

kalian.c

![Teks alternatif](https://i.ibb.co/L6b4XPD/Whats-App-Image-2023-05-11-at-7-21-57-PM.jpg)

cinta.c

![Teks alternatif](https://i.ibb.co/yfjTmsr/Whats-App-Image-2023-05-11-at-7-34-35-PM.jpg)

sisop.c

![Teks alternatif](https://i.ibb.co/N11TrDh/Whats-App-Image-2023-05-11-at-7-37-42-PM.jpg)

__Kendala__

Kendalanya awalnya faktorial tidak langsung menunjukkan hasil yang benar harus diganti progam lebih detail dulu baru menampilkan output yang benar. 


# Soal 3

 __Analisa Soal__


Pada soal ini diminta membuat 2 progam c yaitu stream.c dan user.c dimana nanri user.c akan mengirimkan perintah dan stream.c menjalankan perintah. Pada soal diperintahkan untuk mendekrip lagu dari file json dan hasilnya di masukkan di playlist.txt apabila mengirimkan perintah DECRYPT dan akan menampilkan list lagu yang telah di dekrip apabila mengirimkan perintah LIST dan  apabila mengirimkan perintah selain itu akan mengeluarkan output UNKNOWN COMMAND. 


__Code Progam__

1. stream.c
``` 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <math.h>
#include <unistd.h>

#define kuantitas_lagu 2000
#define kuantitas_ 2000

struct Message {
    long messageType;
    char output_teks[kuantitas_];
};

int metoderot13(char *order) {
    int indeks_ = 0;
    char kata_;

    while ((kata_ = order[indeks_++])) {
        if (kata_ >= 'A' && kata_ <= 'Z') {
            kata_ = ((kata_ - 'A' + 13) % 26) + 'A';
        } else if (kata_ >= 'a' && kata_ <= 'z') {
            kata_ = ((kata_ - 'a' + 13) % 26) + 'a';
        }
        order[indeks_-1] = kata_;
    }

    return 0;
}

int charbase64(char c) {
    if (c >= 'A' && c <= 'Z') return c - 'A';
    if (c >= 'a' && c <= 'z') return c - 'a' + 26;
    if (c >= '0' && c <= '9') return c - '0' + 52;
    if (c == '+') return 62;
    if (c == '/') return 63;
    return -1;  
}

int metodebase64(char *teks) {
    int i = 0, j = 0;
    while (teks[i]) {
        int nilai = charbase64(teks[i++]);
        if (nilai != -1) teks[j++] = teks[i-1];
    }
    teks[j] = '\0';

    int len = strlen(teks);
    int man = (teks[len - 1] == '=') + (teks[len - 2] == '=');

    len = len * 3 / 4 - man;

    char *out = calloc(len + 1, sizeof(char));

    for (i = 0, j = 0; i < len; i += 3, j += 4) {
        unsigned long n = (charbase64(teks[j]) << 18) | (charbase64(teks[j+1]) << 12) | (charbase64(teks[j+2]) << 6) | charbase64(teks[j+3]);
        out[i] = (n >> 16) & 0xFF;
        out[i+1] = (n >> 8) & 0xFF;
        out[i+2] = n & 0xFF;
    }

    memcpy(teks, out, len);
    teks[len] = '\0';

    free(out);

    return 0;
}


int metodehex(char *teks) {
    int i = 0, j = 0;
    size_t len = strlen(teks);

    for (i = 0; i < len; i += 2) {
        int go = 0;

        for (int k = 0; k < 2 && isxdigit(teks[i + k]); k++) {
            go = (go << 4) | (isdigit(teks[i+k]) ? teks[i+k]-'0' : tolower(teks[i+k])-'a'+10);
        }

        teks[j++] = go & 0xFF;
    }

    teks[j] = '\0';

    return 0;
}


typedef int (*DecryptMethod)(char*);
const DecryptMethod metodedekrip[] = {
    metoderot13,
    metodebase64,
    metodehex
};

const char *jenismetode[] = {
    "rot13",
    "base64",
    "hex"
};

int mendekriplagu(char *method, char *song) {
    
    int i;
    for (i = 0; i < sizeof(jenismetode) / sizeof(jenismetode[0]); i++) {
        if (strcmp(method, jenismetode[i]) == 0) {
            break;
        }
    }
   
   
    if (i < sizeof(jenismetode) / sizeof(jenismetode[0])) {
        return metodedekrip[i](song);
    } else {
        printf("Metode tidak diketahui: %s\n", method);
        return -1;
    }
}

char songs[kuantitas_lagu][kuantitas_];

int bandingkan(const void *a, const void *b) {
    return strcmp((const char *)a, (const char *)b);
}

int filesort() {
    FILE *Input_file, *Output_file;
    char line[kuantitas_];
    char method[kuantitas_];
    char song[kuantitas_];
    char key[] = " \"method\": \"";
    char keyEnd[] = "\",";
    int countlagu = 0;

    Input_file = fopen("song-playlist.json", "r");
    if (Input_file == NULL) {
        perror("gagal membuka song-playlist.json ");
        return -1;
    }

    while (fgets(line, sizeof(line), Input_file) != NULL) {
       
        char *pMethod = strstr(line, key);
        if (pMethod == NULL) {
            continue;
        }
        
        strncpy(method, pMethod + strlen(key), kuantitas_);
       
        char *pQuote = strstr(method, keyEnd);
        if (pQuote == NULL) {
            continue;
        }
        
        *pQuote = '\0';

    
        char *pSong = strstr(line, " \"song\": \"");
        if (pSong == NULL) {
            continue;
        }
        
        strncpy(song, pSong + strlen(" \"song\": \""), kuantitas_);
      
        pQuote = strstr(song, "\"");
        if (pQuote == NULL) {
            continue;
        }
      
        *pQuote = '\0';

       
        mendekriplagu(method, song);

       
        strncpy(songs[countlagu], song, kuantitas_);
        countlagu++;
    }

    fclose(Input_file);

    
    if (countlagu == 0) {
        printf("Tidak ada lagu yang bisa didekripsi.\n");
        return 0;
    }

   
    qsort(songs, countlagu, kuantitas_ * sizeof(char), bandingkan);

    Output_file = fopen("playlist.txt", "w");
    if(Output_file == NULL) {
        perror("tidak bisa membuka playlist.txt");
        return -1;
    }

    
    fprintf(Output_file, "Playlist yang telah di dekrip:\n");
    for (int i = 0; i < countlagu; i++) {
        fprintf(Output_file, "- %s\n", songs[i]);
    }
    fclose(Output_file);

    printf("Playlist masuk 'playlist.txt'\n");

    return 0;
}

int perintahlist() {
    const char* nama_file = "playlist.txt";
    char buffer[kuantitas_];
    memset(buffer, 0, sizeof(buffer));

    FILE *Output_file = fopen(nama_file, "r");
    if (!Output_file) {
        printf("Tidak bisa membuka file %s\n", nama_file);
        return -1;
    }

    int lineCount = 0;
    while (fgets(buffer, kuantitas_, Output_file)) {
        lineCount++;
    }

    printf("Daftar Lagu terdekripsi (%d):\n", lineCount);

    rewind(Output_file); 
    while (fgets(buffer, kuantitas_, Output_file)) {
        printf("%s", buffer);
    }

    fclose(Output_file);
    return 0;
}

int perintahTidakDikenal() {
    printf("UNKNOWN COMMAND\n");
    return 0;
}

int main() {
    int msgid = msgget(ftok(".", 's'), 0666 | IPC_CREAT);
    if (msgid == -1) {
        perror("msgget");
        exit(1);
    }

    for (;;) {
        struct Message msg;
        msgrcv(msgid, &msg, sizeof(msg.output_teks), 1, 0);

        puts("Pesan diterima:");
        switch (msg.output_teks[0]) {
            case 'D':
                if (filesort() != 0) {
                    puts("Gagal membuat file playlist.");
                }
                break;
            case 'L':
                if (perintahlist() != 0) {
                    puts("Gagal menampilkan daftar lagu.");
                }
                break;
            default:
                puts("Pilihan tidak dapat dilihat.");
        }
    }
    return 0;
} 


```

Penjelasan kode 



struct Message: Mendefinisikan struktur Pesan yang memiliki dua elemen yaitu messageType bertipe long dan output_teks bertipe array of char dengan panjang kuantitas_.

metoderot13(char *order): Fungsi untuk melakukan enkripsi ROT13 pada string order. Enkripsi ROT13 akan mengganti setiap huruf alfabet dengan huruf yang terletak 13 karakter setelahnya dalam urutan alfabet (dalam susunan melingkar).

charbase64(char c): Fungsi untuk mengkonversi karakter c menjadi nilai numerik sesuai standar Base64.

metodebase64(char *teks): Fungsi untuk melakukan decoding base64 pada string teks.

metodehex(char *teks): Fungsi untuk melakukan decoding hex pada string teks.

mendekriplagu(char *method, char *song): Fungsi untuk mendekripsi lagu menggunakan metode yang ditentukan oleh parameter method.

bandingkan(const void *a, const void *b): Fungsi untuk membandingkan dua string (a dan b) dalam proses pengurutan (sorting).

filesort(): Fungsi untuk membaca file "song-playlist.json", mendekripsi lagu-lagu pada file tersebut menggunakan metode yang sesuai, menyimpan lagu-lagu yang berhasil didekripsi dalam array songs, mengurutkan lagu-lagu tersebut secara alfabetis, dan menyimpan list tersebut ke dalam file "playlist.txt".

perintahlist(): Fungsi untuk membuka file "playlist.txt", menampilkan isi dari file tersebut (yaitu daftar lagu-lagu terdekripsi), dan menutup file.

perintahTidakDikenal(): Fungsi untuk menampilkan pesan kesalahan ketika perintah yang dimasukkan tidak dikenali oleh program.

main(): Fungsi utama yang berisi loop untuk menerima pesan dari message queue dan memprosesnya menggunakan fungsi-fungsi di atas. Program ini akan terus berjalan selama tidak ada error atau penghentian paksa.

2. user.c
```

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#define kuantitas_ 2000

struct Message {
    long messageType;
    char output_teks[kuantitas_];
};

int main() {
    int msgid = msgget(ftok(".", 's'), 0666);
    if (msgid == -1) {
        perror("msgget");
        exit(1);
    }

    printf("Selamat datang di sistem stream.\n");
    printf("Untuk melihat daftar perintah, silakan masukkan GO.\n");

    for (;;) {
        char input[kuantitas_];
        printf("\nPerintah: ");
        fgets(input, kuantitas_, stdin);

        // hapus newline character dari input
        input[strcspn(input, "\n")] = 0;

        struct Message msg;
        msg.messageType = 1;
        strncpy(msg.output_teks, input, kuantitas_);

        if (msgsnd(msgid, &msg, sizeof(msg.output_teks), 0) == -1) {
            perror("msgsnd");
            exit(1);
        }

        if (strcmp(input, "GO") == 0) {
            printf("Daftar perintah:\n");
            printf("- DECRYPT: untuk melakukan decrypt pada file song-playlist.json\n");
            printf("- LIST: untuk menampilkan daftar lagu yang telah di-decrypt\n");
            printf("- EXIT: untuk keluar dari program\n");
        } else if (strcmp(input, "EXIT") == 0) {
            printf("Sampai jumpa!\n");
            break;
        }
    }

    return 0;
}


```
Penjelasan kode 
struct Message: Mendefinisikan struktur Pesan yang memiliki dua elemen yaitu messageType bertipe long dan output_teks bertipe array of char dengan panjang kuantitas_.

main(): Fungsi utama yang berisi loop untuk menerima input perintah dari pengguna, memasukkan perintah tersebut ke dalam struktur Pesan, dan mengirimkannya melalui message queue ke sistem stream. Program ini akan terus berjalan selama tidak ada error atau penghentian paksa.

Pada bagian awal fungsi main(), program akan memanggil fungsi msgget() untuk mendapatkan ID message queue. Apabila gagal, program akan menampilkan pesan error dan keluar dari program.

__Testing Output__

stream.c

![Teks alternatif](https://i.ibb.co/jgjL0Rg/Whats-App-Image-2023-05-13-at-5-08-41-PM.jpg)

user.c

![Teks alternatif](https://i.ibb.co/rd6bcmW/Whats-App-Image-2023-05-13-at-5-09-28-PM.jpg)

playlist.txt

![Teks alternatif](https://i.ibb.co/bbRpH7M/Whats-App-Image-2023-05-13-at-5-10-15-PM.jpg)

__Kendala__

Saya hanya bisa mengerjakan poin a,b,c dan g saja

# Soal 4

 __Analisa Soal__  
 Diminta membuat 3 program. Program pertama untuk mendownlod zip lalu mengekstraknya. program ke 2 diminta untuk mengkategorikan sesuai nama ekstension yang ada di ekstension.txt dengan maksimal 1 folder memiliki sejumlah file yang ada di max.txt. jika sudah sebanyak yang maksimal maka membuat folder baru dengan tambahan angka di kurung. jika ekstensi tidak ada di ekstension.txt maka masukkan ke folder other. kemudian menghitung banyaknya file di setiap ekstension tersebut termasuk other, lalu mencatat setiap membuat dan mengakses folder, dan memindahkan file ke log.txt. kemudian juga menghitung banyaknya file tiap ekstensinya termasuk folder. Program ke 3 diminta untuk mengekstrak informasi dari log.txt sesuai ketentuan

 __Code Program__  
 - unzip.c
 ```
 #include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <errno.h>

void download_file(char* url, char* filename) {
    pid_t pid = fork();
    if (pid == 0) {
        // child process
        execlp("wget", "wget", "-O", filename, url, NULL);
        exit(EXIT_SUCCESS);
    } else if (pid > 0) {
        // parent process
        int status;
        waitpid(pid, &status, 0);
    } else {
        // fork failed
        perror("fork failed");
        exit(EXIT_FAILURE);
    }
}

void unzip_file(char *filename) {
    pid_t pid = fork();
    if (pid == 0) {
        execlp("unzip", "unzip", filename, NULL);
        perror("Failed to unzip file");
        exit(1);
    } else if (pid < 0) {
        perror("Failed to create child process");
        exit(1);
    } else {
        int status;
        waitpid(pid, &status, 0);
        if (WIFEXITED(status) && WEXITSTATUS(status) != 0) {
            exit(1);
        }
    }
}

int main() {
    char *url = "https://drive.google.com/uc?export=download&id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp";
    char *filename = "hehe.zip";

    download_file(url, filename);
    unzip_file(filename);

    return 0;
}
```
Penjelasan program:  
mendownload dan mengekstrak
- categorize.c
```
#include <ctype.h>
#include <dirent.h>
#include <libgen.h>
#include <math.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>

#define MAX_EXTENSION 100

int* extension_file_num;
int other_extension_num;

typedef struct ThreadArgument {
    char* dirname;
    char** extensions;
    int count_extension;
    int max;
} ThreadArgument;

void makeDir(char* dirname);
void move(char* src, char* dest);
char* formattedTime();

void logAccess(char* path);
void logMove(char* ext, char* src, char* dest);
void logMade(char* folder);

void fileProcess(char* path, int num_extension, char** extensions, int max);
void directoryProcess(char* dirname, int num_extension, char** extensions, int max);
void* directory_process_routine(void* arg);
void printExtFileCount(int ext_file_num[], char** extensions, int n_ext);

int main() {
    extension_file_num = calloc(MAX_EXTENSION, sizeof(int));
    other_extension_num = 0;
    char *extensions[MAX_EXTENSION], c;
    int num_extension = 0;
    int pos = 0;
    int ext_max;

    FILE* file = fopen("extensions.txt", "a");
    fprintf(file, "other\n");
    fclose(file);

    file = fopen("extensions.txt", "r");

    char line[MAX_EXTENSION];
    while (fgets(line, MAX_EXTENSION, file)) {
        char* token = strtok(line, " \t\r\n");
        while (token != NULL) {
            token[strcspn(token, "\n")] = 0;
            extensions[num_extension] = strdup(token);
            num_extension++;
            token = strtok(NULL, " \t\r\n");
        }
    }

    file = fopen("max.txt", "r");
    fscanf(file, "%d", &ext_max);
    fclose(file);

    directoryProcess("files", num_extension, extensions, ext_max);
    printExtFileCount(extension_file_num, extensions, num_extension);

    return 0;
}

void makeDir(char* dirname) {
    char command[1000];
    sprintf(command, "mkdir %s", dirname);
    system(command);
}

void move(char* src, char* dest) {
    char command[1000];
    sprintf(command, "cp %s %s", src, dest);
    system(command);
}

char* formattedTime() {
    time_t current_time = time(NULL);
    struct tm* time_info = localtime(&current_time);

    char* formatted_time = malloc(20 * sizeof(char));
    strftime(formatted_time, 20, "%Y-%m-%d %H:%M:%S", time_info);
    return formatted_time;
}

void logAccess(char* path) {
    FILE* logFile = fopen("log.txt", "a");
    fprintf(logFile, "%s ACCESSED %s\n", formattedTime(), path);
    fclose(logFile);
}

void logMove(char* ext, char* src, char* dest) {
    FILE* logFile = fopen("log.txt", "a");
    fprintf(logFile, "%s MOVED %s file : %s > %s\n", formattedTime(), ext, src, dest);
    fclose(logFile);
}

void logMade(char* folder) {
    FILE* logFile = fopen("log.txt", "a");
    fprintf(logFile, "%s MADE %s\n", formattedTime(), folder);
    fclose(logFile);
}

void fileProcess(char* path, int num_extension, char** extensions, int max) {
    if (access("categorized", F_OK) != 0) {
        makeDir("categorized");
        logMade("categorized");
    }

    char* filename = basename(path);
    char* lastDot = strrchr(filename, '.');
    char command[1000];

    if (lastDot) {
        *lastDot = '\0';

        char ext[150], ext2[150];
        strcpy(ext, lastDot + 1);
        strcpy(ext2, ext);
        for (int i = 0; ext2[i]; i++) ext2[i] = tolower(ext[i]);

        int ext_idx = -1;
        for (int i = 0; i < num_extension; i++) {
            if (!strcmp(ext2, extensions[i])) {
                ext_idx = i;
                extension_file_num[ext_idx]++;
                break;
            }
        }

        char newPath[200], oldPath[200];
     
        if (ext_idx >= 0 && extension_file_num[ext_idx] <= max) {
            sprintf(newPath, "categorized/%s", extensions[ext_idx]);
        } else if (ext_idx >= 0) {  
            sprintf(newPath, "categorized/%s (%d)", extensions[ext_idx], (int)ceil(extension_file_num[ext_idx] / (float)max));
        } else {  
            extension_file_num[num_extension - 1]++;
            sprintf(newPath, "categorized/other");
        }

       
        if (access(newPath, F_OK) != 0) {
            sprintf(command, "mkdir \"%s\"", newPath);
            system(command);
            logMade(newPath);
        }

        snprintf(oldPath, sizeof(oldPath), "%s.%s", path, ext);
        sprintf(command, "cp '%s' \"%s\"", oldPath, newPath);
        system(command);
        logMove(ext, oldPath, newPath);
    } else {  
        extension_file_num[num_extension - 1]++;
        if (access("categorized/other", F_OK) != 0) {
            system("mkdir categorized/other");
            logMade("categorized/other");
        }

        sprintf(command, "cp '%s' categorized/other", path);
        system(command);
        logMove("other", path, "categorized/other");
    }
}

void directoryProcess(char* dirname, int num_extension, char** extensions, int max) {
    pthread_t myThreads[200];
    int n_thread = 0;

    DIR* dir;
    struct dirent* entry;
    dir = opendir(dirname);
    logAccess(dirname);

    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
            continue;
        }
        char path[2000];
        sprintf(path, "%s/%s", dirname, entry->d_name);

        if (entry->d_type == DT_DIR) {
            struct ThreadArgument* threadArg = malloc(sizeof(struct ThreadArgument));
            threadArg->dirname = strdup(path);
            threadArg->count_extension = num_extension;
            threadArg->extensions = extensions;
            threadArg->max = max;

            pthread_create(&myThreads[n_thread], NULL, directory_process_routine, threadArg);
            n_thread++;
        } else { 
            fileProcess(path, num_extension, extensions, max);
        }
    }
    closedir(dir);

    for (int i = 0; i < n_thread; i++) {
        pthread_join(myThreads[i], NULL);
    }
}

void* directory_process_routine(void* arg) {
    struct ThreadArgument* threadArg = (struct ThreadArgument*)arg;
    directoryProcess(threadArg->dirname, threadArg->count_extension, threadArg->extensions, threadArg->max);
    pthread_exit(NULL);
}

void printExtFileCount(int ext_file_num[], char** extensions, int n_ext) {

    for (int i = 0; i < n_ext - 1; i++) {
        for (int j = i + 1; j < n_ext; j++) {
            if (ext_file_num[i] > ext_file_num[j]) {
                int temp = ext_file_num[i];
                ext_file_num[i] = ext_file_num[j];
                ext_file_num[j] = temp;

                char temp2[10];
                strcpy(temp2, extensions[i]);
                strcpy(extensions[i], extensions[j]);
                strcpy(extensions[j], temp2);
            }
        }
    }
    for (int i = 0; i < n_ext; i++) {
        printf("%s : %d\n", extensions[i], ext_file_num[i]);
    }
}
```
Penjelasan Program:  
Menambahkan other ke ekstension.txt, mengecek isi ekstension.txt dan max.txt, mengecek file di folder files dan mengcopy sesuai ketentuan. setiap mengcopy menambahkan jumlah file di ekstention, setiap melakukan sesuatu yang diperlukan membuat log, maka menambakan log ke log.txt, mengsort ektstention berdasarkan jumlah filenya, print. 
- logchecker.c
```
#include <stdio.h>
#include <string.h>

int countAccessedEvents(char *filename) {
    char accessed[] = "ACCESSED";
    char line[100];
    int count = 0;
    
    FILE* file = fopen(filename, "r");
    if (file == NULL) {
        printf("Error opening file\n");
        return -1;
    }
    
    while (fgets(line, sizeof(line), file)) {
        if (strstr(line, accessed)) {
            count++;
        }
    }
    
    fclose(file);
    
    return count;
}

int main() {
    char filename[] = "log.txt";
    int count = countAccessedEvents(filename);
    
    if (count >= 0) {
        printf("Number of ACCESSED events: %d\n", count);
    }
    
    return 0;
}
```
Penjelasan Program:  
Membuka file log.txt kemudian menghitung jumlah ACCESSED  
__Output__
- unzip  
![unzip](https://i.ibb.co/WsN7hYp/image.png)
- categorize  
![categorize](https://i.ibb.co/0csrs32/image.png)
- logchecker  
![logchecker](https://i.ibb.co/Qf55gMj/image.png)

__Kendala__
Kesulitan untuk membuat jika sudah max membuat folder dengan nama ekstension(angka dari 2), dan penggunaan multithreading. Dan tidak sempat untuk menyelesaikan logchecker sampai poin akhir
